/*
===========================================================================================================================

        ___ ___              __    
 ___   / _// _/____ ___  ___/ /___ 
/ _ \ / _// _// __// _ \/ _  // -_)
\___//_/ /_/  \__/ \___/\_,_/ \__/ 


==================================
REACT JS - APIs
==================================

Crear una aplicación para buscar y obtener posters de películas y links a IMDB, utilizando la API de OMDB (http://www.omdbapi.com/)
La app contiene un resultado de ejemplo como guía para maquetado.

En base al diseño provisto, generar resultados contra la búsqueda ingresada por el usuario.


Reglas básicas:
- No hay directivas en la cantidad de componentes a utilizar. Se puede resolver en un solo componente o usando sub-componentes.
- En lo posible, capturar las respuestas fallidas o de error de la API.


Tips:
- Para utilizar OMDB es necesario crear una API KEY.
- El proyecto incluye algunas librerias que pueden ser de utildad para el ejercicio. Revisar.

===========================================================================================================================
 */

import React, { useState, useEffect } from "react";
import Form from './Form';
import Posters from "./Posters";






function Main() {

  const [busqueda, guardarBusqueda] = useState ('');
  const [poster, setPoster] = useState ({});

  const consultarAPI = async() => {

    if(busqueda === '') return;

    const url = `http://www.omdbapi.com/?&apikey=f737bbc1&t=${busqueda}`;
    const respuesta = await fetch(url);
    const resultado = await respuesta.json()

  
    
   setPoster(resultado)

  }

  useEffect (()=>{

      consultarAPI()     
    
  },[busqueda])

 console.log(poster)
  


    return (
    <div className="container-fluid" style={{margin:0, padding:0}}>
      <div className="row">
        <div className="col text-center">
          <div className="mainDiv">
       
           

             <Form
               guardarBusqueda={guardarBusqueda}
             />  
            
             <Posters
             poster={poster}
             />
            
            
              
            </div>
          </div>
        </div>
      </div>
   
  );
}

export default Main;
