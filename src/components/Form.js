import React, { useState } from "react";
import Error from "./Error";

function Form({ guardarBusqueda }) {
  const [terminoBusqueda, guardarTerminoBusqueda] = useState("");
  const [error, guardarError] = useState(false);

  const buscarTitulo = (e) => {
    e.preventDefault();

    //Validar busqueda / Error
    if (terminoBusqueda === "") {
      guardarError(true);
      return;
    }

    guardarError(false);
    guardarBusqueda(terminoBusqueda);
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="col text-center">
        <div className="mainDiv">
          <h2 className="text-white">OMDB</h2>
          <h4 className="text-white">BUSQUEDAssss</h4>

          <form action="" className="form-inline" onSubmit={buscarTitulo}>
            <div className="form-group">
              <input
                type="text"
                placeholder="Titulo"
                className="form-control"
                name="Title"
                onChange={(e) => guardarTerminoBusqueda(e.target.value)}
              />
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-success">
                Buscar
              </button>
            </div>
          </form>
          <div className="error" style={{ marginTop: "10px" }}>
            {error ? <Error mensaje="Introducir Titulo Pelicula" /> : null}
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Form;
