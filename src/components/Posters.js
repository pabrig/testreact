import React from "react";

function Posters({poster}) {



  return (
    <div className="card text-white bg-secondary " style={{ marginTop:"10px", padding:"3em", margin:"3em"}}>
    <div className="card-header"> <h3 className="card-title">{poster.Title}</h3></div>
    <div className="card-body">
      <h4 className="card-title">{poster.Genre}</h4>
      <img
          className="card-img-top"
          alt=" "
          src={poster.Poster}
            />
    </div>
    <div className="card-body">
          <a href= " " className="btn btn-primary">
            Go to IMDB
          </a>
    </div>
  </div>
   
  );
}

export default Posters;
