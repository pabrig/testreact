import React from "react";

function Error({ mensaje }) {
  return (
   
    <div class="alert alert-dismissible alert-danger">
             <strong>{mensaje}</strong>.
    </div>
  );
}

export default Error;
